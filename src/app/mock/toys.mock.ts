import {Toy} from "../toy-list/toy/toy";
export const TOYS:Toy[] = [
  {
    name: 'Teddy Bear',
    price: 25,
    description: 'A cuddly teddy bear.',
    manufacturer: 'Teddy Co.', // This toy has a manufacturer
  },
  {
    name: 'Race Car',
    price: 45,
    description: 'A fast race car.',
    // This toy does not have a manufacturer
  },
  // Add more toys here...
];
