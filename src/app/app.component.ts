import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {ToyListComponent} from "./toy-list/toy-list.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, ToyListComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'angular-interfaces';
}
