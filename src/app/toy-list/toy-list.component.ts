import {Component} from '@angular/core';
import {ToyComponent} from "./toy/toy.component";
import {NgForOf, NgIf} from "@angular/common";
import {TOYS} from "../mock/toys.mock";

@Component({
  selector: 'app-toy-list',
  standalone: true,
  imports: [
    ToyComponent,
    NgForOf,
    NgIf
  ],
  templateUrl: './toy-list.component.html',
  styleUrl: './toy-list.component.scss'
})
export class ToyListComponent {
  protected readonly toys = TOYS;
}
