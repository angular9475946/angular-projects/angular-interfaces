export interface Toy {
  name: string;
  price: number;
  description: string;
  manufacturer?: string; // This is an optional parameter
}
