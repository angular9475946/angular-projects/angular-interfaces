import {Component, Input} from '@angular/core';
import {Toy} from "./toy";
import {NgForOf, NgIf} from "@angular/common";

@Component({
  selector: 'app-toy',
  standalone: true,
  imports: [
    NgForOf,
    NgIf
  ],
  templateUrl: './toy.component.html',
  styleUrl: './toy.component.scss'
})
export class ToyComponent {
  @Input() toys!: Toy;
  @Input() toy!: Toy;
}
